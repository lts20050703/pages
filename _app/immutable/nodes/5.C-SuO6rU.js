import{h as s,a,t as o}from"../chunks/disclose-version.yLpB9axd.js";import{f as h,p as c,a as r,t as u,$ as g,b as m}from"../chunks/runtime.DpLSP5SF.js";import{T as d,n,v as b}from"../chunks/template.Dz6SxDos.js";var f=o(`<!> <div class="mt-4 flex flex-col items-center justify-center gap-4"><div class="text-center text-3xl">Manual (Documentation)</div> <ul class="w-[64rem] max-w-full list-inside list-disc"><li>Dòng đầu: Tiêu đề (Tên đề thi, tên bộ đề ôn, ...)</li> <div class="flex items-center justify-center"><hr class="my-4 w-96 max-w-full"></div> <li>Từ dòng 2 trở đi:</li> <li class="ml-4">Để khai báo phần mới (ví dụ như đề thi tiếng anh thường có nhiều phần), bắt đầu dòng đó bằng
			"#"</li> <li class="ml-8">Ví dụ: # Mark the letter A, B, C, or D on your answer sheet to indicate the word whose
			underlined part differs from the other three in pronunciation in each of the following
			questions.</li> <li class="ml-8">Nếu không muốn các câu trong phần đó bị xáo trộn thứ tự câu, bắt đầu dòng đó bằng 📌, ví dụ
			như phần đọc văn bản và chọn từ điền vào ô trống thường không xáo lại thứ tự câu, chỉ xáo thứ
			tự đáp án thì nên khai báo bằng 📌.</li> <li class="ml-8">Nếu đề thi không có chia ra nhiều phần giống đề thi tiếng anh thì không cần phải khai báo phần
			mới.</li> <li class="ml-4">Để khai báo một câu hỏi mới, bắt đầu dòng đó bằng "câu" hoặc "question", theo sau đó là một
			con số. Ví dụ như "câu 1" hoặc "question 1"</li> <li class="ml-4">Để khai báo một câu trả lời A, B, C, hoặc D, bắt đầu dòng đó bằng "A", "B", "C" hoặc "D", theo
			sau đó là dấu "." hoặc ":". Ví dụ như "A." hoặc "D:"</li> <li class="ml-8">Để khai báo một câu trả lời là đúng, bắt đầu dòng đó bằng "*", theo sau đó là "A", "B C", "D"
			và dấu "." hoặc ":". Ví dụ như "*B." hoặc "*C:"</li> <li class="ml-8">Lưu ý: Hiện tại app chỉ hỗ trợ 4 đáp án ABCD</li> <div class="flex items-center justify-center"><hr class="my-4 w-96 max-w-full"></div> <li>Để in đậm, bắt đầu in đậm bằng &ltb&gt và kết thúc bằng &lt/b&gt. Ví dụ như "Question 20:
			&ltb&gtVariations&lt/b&gt in the color of sea under water from blue to green seem to be caused
			by high or low concentrations of salt.".</li> <li>Để gạch dưới, bắt đầu gạch dưới bằng &ltu&gt và kết thúc bằng &lt/u&gt. Ví dụ như "Question
			21: Watermelon crops must be rotated &ltu&gt regularly&lt/u&gt with other crops to avoid
			fungal disease."</li> <li>Để in nghiên, bắt đầu in nghiên bằng &lti&gt và kết thúc bằng &lt/i&gt. Ví dụ như "Question
			22: He is regarded as one of the most &lti&gt distinguished&lt/i&gt scientists of all
			generations."</li></ul></div>`,1);function w(i,e){r(e,!0);var t=f();s(p=>{u(()=>g.title=n.aozta)});var l=h(t);d(l,{href:"../",get name(){return n.aozta},get version(){return b.aozta}}),m(2),a(i,t),c()}export{w as component};
