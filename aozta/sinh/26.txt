﻿BÀI 26 THUYẾT TIẾN HÓA TỔNG HỢP HIỆN ĐẠI
https://azota.vn/de-thi/qgvstq
Câu 1. Theo thuyết tiến hóa hiện đại, nhân tố nào dưới đây không được xem là nhân tố tiến hóa?
A. Các yếu tố ngẫu nhiên.
B. Chọn lọc tự nhiên.
*C. Giao phối ngẫu nhiên.
D. Giao phối không ngẫu nhiên.
Câu 2. Theo quan niệm tiến hóa hiện đại, nhân tố tiến hóa nào có thể làm tăng sự đa dạng di truyền trong quần thể?
A. Các yếu tố ngẫu nhiên.
*B. Đột biến.
C. Giao phối không ngẫu nhiên.
D. Chọn lọc tự nhiên.
Câu 3. Nếu chỉ xét riêng từng nhân tố thì nhân tố tiến hóa nào sau đây làm thay đổi tần số alen của quần thể với tốc độ chậm nhất?
A. Chọn lọc tự nhiên.
B. Các yếu tố ngẫu nhiên.
*C. Đột biến gen.
D. Di - nhập gen.
Câu 4. Khi nói về nhân tố di - nhập gen, phát biểu nào sau đây đúng?
A. Di - nhập gen có thể làm thay đổi tần số alen nhưng không làm thay đổi thành phần kiểu gen của quần thể.
*B. Sự phát tán hạt phấn ở thực vật chính là một hình thức di - nhập gen.
C. Di - nhập gen luôn mang đến cho quần thể những alen có lợi.
D. Di - nhập gen luôn làm thăng tần số alen trội của quần thể.
Câu 5. Trong các nhân tố sau, nhân tố tiến hóa nào sau đây là nhân tố tiến hóa có hướng?
*A. Chọn lọc tự nhiên.
B. Đột biến.
C. Di nhập gen.
D. Yếu tố ngẫu nhiên.
Câu 6. Theo thuyết tiến hóa hiện đại, phát biểu nào sau đây là sai?
*A. Di nhập gen định hướng cho quá trình tiến hóa của quần thể.
B. Yếu tố ngẫu nhiên có thể loại bỏ một alen có lợi ra khỏi quần thể.
C. Đột biến cung cấp nguyên liệu sơ cấp cho tiến hóa.
D. Chọn lọc tự nhiên đào thải alen trội làm thay đổi tần số alen trội của quần thể nhanh hơn đào thải alen lặn.
Câu 7. Theo quan điểm hiện đại về chọn lọc tự nhiên, phát biểu nào sau đây không đúng?
A. Chọn lọc tự nhiên quy định chiều hướng và nhịp điệu biến đổi thành phần kiểu gen của quần thể.
B. Khi môi trường thay đổi theo một hướng xác định thì chọn lọc tự nhiên sẽ làm biến đổi tần số alen theo một hướng xác định.
C. Chọn lọc tự nhiên về thực chất là quá trình phân hóa khả năng sống sót và khả năng sinh sản của các cá thể với kiểu gen khác nhau trong quần thể.
*D. Chọn lọc tự nhiên chỉ tác động lên từng alen và làm thay đổi tần số kiểu gen của quần thể.
Câu 8. Đối với quá trình tiến hóa, yếu tố ngẫu nhiên có vai trò?
*A. Làm biến đổi mạnh tần số alen của những quần thể có kích thước nhỏ.
B. Làm tăng sự đa dạng di truyền của quần thể sinh vật.
C. Làm thay đổi tần số alen và thành phần kiểu gen của quần thể theo một hướng xác định.
D. Chỉ đào thải các alen có hại và giữ lại các alen có lợi cho quần thể.
Câu 9. Ở một loài thực vật giao phấn, các hạt phấn của quần thể 1 theo gió bay sang quần thể 2 và thụ phấn cho các cây ở quần thể 2. Đây là ví dụ về?
A. Biến động di truyền.
*B. Di - nhập gen.
C. Yếu tố ngẫu nhiên.
D. Giao phối không ngẫu nhiên.
Câu 10. Theo quan niệm hiện đại, loại biến dị nào sau đây là nguồn nguyên liệu sơ cấp cho quá trình tiến hóa?
A. Biến dị tổ hợp.
B. Thường biến.
C. Yếu tố ngẫu nhiên.
*D. Đột biến gen.
Câu 11. Theo quan niệm của thuyết tiến hóa hiện đại, phát biểu nào sau đây về chọn lọc tự nhiên là SAI?
A. Chon lọc tự nhiên tác động trực tiếp lên kiểu hình và qua đó gián tiếp tác động lên vốn gen của quần thể.
B. Chọn lọc tự nhiên không bao giờ loại bỏ hết alen lặn ra khỏi quần thể.
*C. Kết quả của chọn lọc tự nhiên là hình thành cá thể mang kiểu hình thích nghi với môi trường.
D. Chọn lọc chống lại alen trội có thể nhanh chóng làm thay đổi tần số alen của quần thể.
Câu 12. Trong quá trình tiến hóa, nhân tố làm thay đổi tần số alen của quần thể chậm nhất là?
*A. Đột biến.
B. Các yếu tố ngẫu nhiên.
C. Chọn lọc tự nhiên.
D. Di - nhập gen.
Câu 13. Theo quan niệm của thuyết tiến hóa tổng hợp, phát biểu nào sau đây sai?
A. Hình thành loài nhờ cơ chế lai xa và đa bội hóa là con đường hình thành loài nhanh nhất.
B. Quần thể sẽ không tiến hóa nếu tần số alen và thành phần kiểu gen của quần thể được duy trì không đổi từ thế hệ này sang thế hệ khác.
*C. Các loài sinh sản vô tính tạo ra số lượng cá thể con cháu rất nhiều và nhanh nên khi môi trường có biến động mạnh sẽ không bị chọn lọc tự nhiên đào thải hàng loạt.
D. Tiến hóa nhỏ và tiến hóa lớn không độc lập nhau mà liên quan mật thiết.
Câu 14. Các nhân tố tiến hóa nào sau đây vừa làm thay đổi tần số alen vừa có thể làm phong phú vốn gen của quần thể?
*A. Đột biến và di - nhập gen.
B. Chọn lọc tự nhiên và các yếu tố ngẫu nhiên.
C. Đột biến và giao phối không ngẫu nhiên.
D. Chọn lọc tự nhiên và giao phối không ngẫu nhiên.
Câu 15. Theo thuyết tiến hóa tổng hợp hiện đại, phát biểu nào sau đây đúng khi nói về quá trình tiến hóa nhỏ?
A. Tiến hóa nhỏ diễn ra trong thời gian lịch sử lâu dài.
*B. Tiến hóa nhỏ làm thay đổi cấu trúc di truyền của quần thể.
C. Tiến hóa nhỏ diễn ra trên quy mô loài và diễn biến không ngừng.
D. Tiến hóa nhỏ giúp hình thành các đơn vị phân loại trên loài.
Câu 16. Giao phối ngẫu nhiên không được xem là nhân tố tiến hóa vì?
*A. Không làm thay đổi vốn gen của quần thể.
B. Làm thay đổi tần số kiểu gen của quần thể.
C. Làm quần thể thay đổi tần số alen.
D. Làm thay đổi định hướng vốn gen của quần thể.
Câu 17. Nhân tố nào làm giảm kích thước của quần thể đáng kể và làm cho vốn gen của quần thể khác hẳn so với quần thể ban đầu?
*A. Các yếu tố ngẫu nhiên.
B. Đột biến.
C. Giao phối ngẫu nhiên.
D. Giao phối không ngẫu nhiên.
Câu 18. Trong tiến hóa nhỏ, nhân tố đột biến có bao nhiêu đặc điểm trong những đặc điểm sau đây?
(1) Làm thay đổi tần số alen và thành phần kiểu gen của quần thể theo một hướng xác định.
*(2) Làm phát sinh các biến dị di truyền của quần thể, cung cấp nguồn nguyên liệu sơ cấp cho quá trình tiến hóa.
(3) Có thể loại bỏ hoàn toàn một alen nào đó khỏi quần thể cho dù alen đó là có lợi.
*(4) Làm thay đổi tần số alen và thành phần kiểu gen của quần thể rất chậm.
A. 1.
B. 4.
C. 3.
*D. 2.
Câu 19. Theo thuyết tiến hóa hiện đại, khi nói về nhân tố tiến hóa có bao nhiêu phát biểu sau đây đúng?
(1) Nếu có sự di - nhập gen chắc chắn làm giảm tần số alen của quần thể.
*(2) Nếu quần thể chịu tác động của các yếu tố ngẫu nhiên có thể làm nghèo vốn gen của quần thể.
*(3) Nếu quần thể chịu tác động của đột biến có thể xuất hiện alen mới.
(4) Chọn lọc tự nhiên tác động trực tiếp lên kiểu gen làm biến đổi tần số alen của quần thể.
A. 4.
B. 3.
C. 1.
*D. 2.
Câu 20. Theo thuyết tiến hóa hiện đại, có bao nhiêu phát biểu sau đây đúng về chọn lọc tự nhiên?
*(1) Chọn lọc tự nhiên tác động trực tiếp lên kiểu hình và giáp tiếp làm biến đổi tần số kiểu gen của quần thể.
*(2) Chọn lọc tự nhiên chống lại alen trội làm biến đổi tần số alen của quần thể nhanh hơn so với chọn lọc chống lại alen lặn.
(3) Chọn lọc tự nhiên làm xuất hiện các alen mới và làm thay đổi tần số alen của quần thể.
(4) Chọn lọc tự nhiên có thể làm biến đổi tần số alen một cách đột ngột không theo một hướng xác định.
A. 3.
B. 4.
C. 1.
*D. 2.