﻿BÀI 24 CÁC BẰNG CHỨNG TIẾN HÓA
https://azota.vn/de-thi/m515xy
Câu 1: Bằng chứng tiến hóa nào sau đây được xem là bằng chứng giải phẫu so sánh? 
A. Tất cả các sinh vật từ đơn bào đến đa bào đều được cấu tạo từ tế bào.  
B. Các axit amin trong chuỗi β-hemoglobin của người và tinh tinh giống nhau.  
*C. Chi trước của mèo và cánh của dơi có các xương phân bố theo thứ tự tương tự nhau.   
D. Di tích của các thực vật sống ở các thời đại trước đã được tìm thấy trong các lớp than đá. 
Câu 2: Vây cá mập, vây cá ngư long (thằn lằn cá) và vây cá voi là ví dụ về: 
A. cơ quan thoái hóa.
*B. cơ quan tương tự.
C. cơ quan tương đồng.
D. phôi sinh học.  
Câu 3: “Các loài có quan hệ họ hàng càng gần thì trình tự các nuclêôtit trong cùng một gen càng có xu hướng giống  nhau và ngược lại” là biểu hiện của bằng chứng: 
A. tế bào học.
*B. sinh học phân tử.
C. phôi sinh học.
D. giải phẫu so sánh. 
Câu 4: Những ví dụ nào sau đây là các cơ quan tương đồng?  
(1) Tuyến nọc độc của rắn và tuyến nước bọt của người.  
(2) Ruột thừa của người và ruột tịt ở động vật  
(3) Gai xương rồng và lá cây lúa.  
(4) Vòi hút của bướm và đôi hàm dưới của bọ cạp.  
(5) Cánh bướm và cánh chim.  
A. 1, 2, 3.
B. 2, 3, 5.
*C. 1, 3, 4.
D. 1, 3, 5.  
Câu 5: Bằng chứng không chứng minh các sinh vật có nguồn gốc chung là: 
A. cơ quan thoái hóa.
B. sự phát triển phôi giống nhau.
C. cơ quan tương đồng.
*D. cơ quan tương tự.  
Câu 6: Bằng chứng nào sau đây vừa phản ánh hướng tiến hóa hội tụ, vừa phản ánh hướng tiến hóa phân li? 
A. Sinh học phân tử.
B. Hóa thạch.
*C. Giải phẫu so sánh.
D. Tế bào học.  
Câu 7: Sự tương đồng về nhiều đặc điểm giải phẫu giữa các loài là những bằng chứng gián tiếp cho thấy các loài sinh  vật hiện nay:  
*A. đều được tiến hóa từ một tổ tiên chung.
B. không có chung nguồn gốc.   
C. tiến hóa theo cùng một hướng giống hệt nhau. 
D. chịu tác động giống hệt nhau từ môi trường.  
Câu 8: Phân tích trình tự nuclêôtit của cùng một loại gen ở các loài có thể cho ta biết: 
*A. mối quan hệ họ hàng giữa các loài đó.  
B. đặc điểm địa chất, khí hậu ở nơi sinh sống của loài đó.  
C. khu vực phân bố địa lí của các loài đó trên Trái Đất.  
D. loài nào xuất hiện trước, loài nào xuất hiện sau trong tiến hóa.  
Câu 9: Bằng chứng quan trọng nhất thể hiện nguồn gốc chung của sinh giới là:  
A. bằng chứng địa lí sinh vật học. 
B. bằng chứng phôi sinh học.  
C. bằng chứng giải phẫu học so sánh. 
*D. tế bào và sinh học phân tử. 
Câu 10: Những bộ phận nào sau đây của cơ thể người được xem là cơ quan thoái hóa?  
I. Trực tràng. II. Ruột già. III. Ruột thừa.  
IV. Răng khôn. V. Xương cùng. VI. Tai  
A. II, III và V.
B. II, IV và V.
*C. III, IV và V.
D. IV, V và VI. 
Câu 11: Bằng chứng sinh học phân tử là những điểm giống và khác nhau giữa các loài về:  
A. cấu tạo trong các nội quan.
B. các giai đoạn phát triển phôi thai.  
*C. trình tự các nucleotit trong các gen tương ứng. 
D. đặc điểm sinh học và biến cố địa chất.
Câu 12: Cấu tạo khác nhau về chi tiết của các cơ quan tương đồng là do:  
A. sự tiến hóa trong quá trình phát triển chung của loài.  
*B. Chọn lọc tự nhiên đã diễn ra theo các hướng khác nhau.  
C. chúng có nguồn gốc khác nhau nhưng phát triển trong những điều kiện giống nhau.  
D. thực hiện các chức phận giống nhau.  
Câu 13: Thành phần axit amin ở chuỗi β-Hemôglôbin ở người và tinh tinh giống nhau chứng tỏ 2 loài này có cùng  nguồn gốc. Đây là ví dụ về:  
A. bằng chứng giải phẫu so sánh.
B. bằng chứng phôi sinh học.
C. bằng chứng địa lí sinh vật học.
*D. bằng chứng sinh học phân tử.  
Câu 14: Khi nói về bằng chứng giải phẫu so sánh, phát biểu nào sau đây là đúng?  
A. Cơ quan tương đồng là những cơ quan có nguồn gốc khác nhau, nằm ở những vị trí tương ứng trên cơ thể, có  kiểu cấu tạo giống nhau.  
B. Trong tiến hóa, các cơ quan tương đồng là bằng chứng quan trọng nhất để phản ánh nguồn gốc chung của sinh  giới.  
*C. Cơ quan tương tự là những cơ quan có nguồn gốc khác nhau nhưng đảm nhiệm những chức phận giống nhau và  có hình thái tương tự nhau.  
D. Cơ quan thoái hóa là cơ quan thay đổi cấu tạo phù hợp với chức năng.  
Câu 15: Khi nói về bằng chứng sinh học phân tử, phát biểu nào sau đây không đúng?  
A. Sự tương đồng về nhiều đặc điểm ở cấp độ phân tử và tế bào cũng cho thấy các loài trên Trái Đất đều có chung tổ  tiên.  
B. Những loài có quan hệ họ hàng càng gần thì trình tự các axit amin trong phân tử protein hay trình tự các nucleotit  trong các gen tương ứng càng có xu hướng giống nhau và ngược lại.  
*C. Phân tích trình tự các axit amin của các loại protein khác nhau hay trình tự các nucleotit của các gen khác nhau ở  các loài có thể cho ta biết mối quan hệ họ hàng giữa các loài.  
D. Các tế bào của tất cả các loài sinh vật hiện nay đều sử dụng chung một loại mã di truyền, đều dùng cùng 20 loại axit  amin để cấu tạo nên protein, … chứng tỏ chúng tiến hóa từ một tổ tiên chung.