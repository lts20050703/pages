﻿ĐỀ SỐ 1
https://azota.vn/de-thi/d2ywip
Câu 1. Chọn câu đúng. Theo tiên đề Bo thì nguyên tử hấp thụ photon khi nguyên tử
A. tồn tại ở trạng thái dừng có mức năng lượng thấp.
*B. chuyển từ trạng thái dừng có mức năng lượng thấp sang trạng thái dừng có mức năng lượng cao hơn.
C. tồn tại ở trạng thái dừng có mức năng lượng cao.
D. chuyển từ trạng thái dừng có mức năng lượng cao sang trạng thái dừng có mức năng lượng thấp hơn.
Câu 2. Ở nguyên tử hiđrô, quỹ đạo nào sau đây có bán kính lớn nhất so với các quỹ đạo còn lại?
A. O	B. N	C. L	*D. P
Câu 3. Trạng thái dừng của nguyên tử là
A. Trạng thái chuyển động đều của ngyên tử
B. Trạng thái đứng yên của ngyên tử
C. Trạng thái trong đó mọi electron của nguyên tử đều không chuyển động đối với hạt nhân
*D. Một trong số các trạng thái có năng lượng xác định, mà nguyên tử có thể tồn tại
Câu 4. Xét nguyên tử hidro theo mẫu nguyên tử Bo. Khi nguyên tử chuyển từ trạng thái dừng có năng lượng -0,85eV sang trạng thái có nănglượng -1,51 eV thì nó phát ra một phôtôn có năng lượng là
A. 2,36eV.	*B. 0,66 eV.	C. - 0,66 eV.	D. -2,36 eV.
Câu 5. Điều nào sau đây không đúng khi nói về quang phổ liên tục?
*A. Quang phổ liên tục là những vạch màu riêng biệt hiện trên một nền tối.
B. Quang phổ liên tục do các vật rắn, lỏng, khí có tỉ khối lớn khi bị nung nóng phát ra.
C. Quang phổ liên tục phụ thuộc vào nhiệt độ của nguồn sáng.
D. Quang phổ liên tục không phụ thuộc vào thành phần cấu tạo của nguồn sáng.
Câu 6. Hạt α ($^4_2He$) có khối lượng 4,0015u, biết số Avô-ga-đrô NA= 6,02.10$^{23}$ mol$^{-1}$, 1u = 931,5 MeV/c$^2$. Biết khối lượng của prôtôn là 1,0073u và khối lượng của nơtron là 1,0087u. Các nuclôn kết hợp với nhau tạo thành hạt α, năng lượng toả ra khi tạo thành 3g khí hêli là
A. 2,7.10$^{12}$J.	B. 2,7.10$^6$ J.	C. 2,05.10$^6$J.	*D. 2,05.10$^{12}$J.
Câu 7. Cho phản ứng hạt nhân: $_{11}^{23}$Na+$_1^2$H→$_2^4$He+$_{10}^{20}$Ne. Lấy khối lượng các hạt nhân $_{11}^{23}$Na; $_{10}^{20}$Ne; $_2^4$He; $_1^1$H lần lượt là 22,9837 u; 19,9869 u; 4,0015 u; 1,0073 u và 1u = 931,5 MeV/c$^2$. Trong phản ứng này, năng lượng
A. thu vào là 3,4524 MeV.	B. thu vào là 2,4219 MeV.
*C. tỏa ra là 2,4219 MeV.		D. tỏa ra là 3,4524 MeV.
Câu 8. Trong thí nghiệm Y-âng về giao thoa với nguồn sáng đơn sắc, hệ vân trên màn có khoảng vân i. Nếu khoảng cách giữa hai khe còn một nửa và khoảng cách từ hai khe đến màn gấp đôi so với ban đầu thì khoảng vân giao thoa trên màn
A. giảm đi bốn lần.	*B. tăng lên bốn lần.	C. tăng lên hai lần.	D. không đổi.
Câu 9.	 Trong nguyên tử hiđrô, bán kính Bo là r$_0$ = 5,3.10$^{-11}$m. Ở một trạng thái kích thích của nguyên tử hiđrô, êlectron chuyển động trên quỹ đạo dừng có bán kính là r = 1,325.10$^{-9}$m. Quỹ đạo đó có tên gọi là quỹ đạo dừng
A. N.	B. M.	*C. O.	D. L.
Câu 10. Phát biểu nào sau đây sai?
A. Tia X làm ion hóa không khí.
*B. Tia X có bước sóng lớn hơn bước sóng của tia tử ngoại.
C. Tia X có bước sóng nhỏ hơn bước sóng của tia tử ngoại.
D. Tia X làm phát quang một số chât.
Câu 11. Trong phản ứng hạt nhân không có sự bảo toàn
A. số nuclôn.	B. năng lượng toàn phần.	C. động lượng.	*D. khối lượng.
Câu 12. Pin quang điện là nguồn điện trong đó
*A. quang năng trực tiếp biến đổi thành điện năng
B. năng lượng Mặt Trời trực tiếp biến đổi thành nhiệt năng
C. một tế bào quang điện được dùng làm máy phát điện
D. một quang điện trở khi được chiếu sáng thì trở thành máy phát điện
Câu 13. Sau những ngày nghỉ mát ở bờ biển, tắm biển và phơi nắng, da ta có thể bị rám nắng hay cháy nắng. Đó là do tác dụng chủ yếu của tia nào sau đây trong ánh sáng Mặt Trời?
A. Tia hồng ngoại.	B. Tia đơn sắc vàng.	C. Tia đơn sắc đỏ.	*D. Tia tử ngoại.
Câu 14. Gọi h là hằng số Plăng, c là tốc độ ánh sáng trong chân không. Chiếu bức xạ có bước sóng λ vào mặt một tấm kim loại có công thoát A thì hiện tượng quang điện xảy ra khi
A. λ=$\frac{3hc}{A}$	B. λ=$\frac{2hc}{A}$	*C. λ<$\frac{hc}{A}$	D. λ>$\frac{4hc}{A}$
Câu 15.	 Bộ phận nào sau đây là một trong ba bộ phận chính của máy quang phổ lăng kính?
A. Mạch biến điệu.	B. Pin quang điện.	C. Mạch tách sóng.	*D. Hệ tán sắc.
Câu 16.	 Nuclôn là tên gọi chung của prôtôn và
A. pôzitron.	B. êlectron.	*C. nơtron.	D. nơtrinô.
Câu 17.	 Phát biểu nào sau đây đúng khi nói về phản ứng hạt nhân?
A. Phản ứng hạt nhân chỉ là sự kết hợp các hạt nhân,dẫn đến sự biến đổi của chúng thành các hạt nhân khác.
B. Phản ứng hạt nhân là sự tác động từ bên ngoài vào hạt nhân làm hạt nhân đó bị vỡ ra.
*C. Phản ứng hạt nhân là mọi quá trình dẫn đến sự biến đổi của hạt nhân.
D. Phản ứng hạt nhân là sự va chạm giữa các hạt nhân.
Câu 18.	 Trong một thí nghiệm Y-âng về giao thoa ánh sáng, 2 khe Y-âng cách nhau 3 mm, hình ảnh giao thoa được hứng trên màn ảnh cách 2 khe 3m. Sử dụng ánh sáng trắng có bước sóng từ 0,40 µm đến 0,75 µm. Trên màn quan sát thu đươc các dải quang phổ. Bề rộng của dải quang phổ bậc 3 là
*A. 1,05 mm	B. 0,55 mm	C. 0,70 mm	D. 0,35 mm
Câu 19.	 Cho: 1eV = 1,6.10$^{-19}$ J; h = 6,625.10$^{-34}$ J.s; c = 3.10$^8$ m/s. Khi êlectrôn trong nguyên tử hiđrô chuyển từ quĩ đạo dừng có năng lượng En = - 3,4 eV sang quĩ đạo dừng có năng lượng Em = - 13,60eV thì nguyên tử phát bức xạ điện từ có bước sóng
A. 0,4860 μm.	*B. 0,1218 μm.	C. 0,0974 μm.	D. 0,6563 μm.
Câu 20.	 Quang điện trở có nguyên tắc hoạt động dựa trên hiện tượng
A. quang – phát quang.	B. quang điện ngoài.	*C. quang điện trong.	D. nhiệt điện.
Câu 21.	 Sự phát quang của nhiều chất rắn có đặc điểm là ánh sáng phát quang có thể kéo dài một khoảng thời gian nào đó sau khi tắt ánh sáng kích thích. Sự phát quang này gọi là?
*A. Sự lân quang.	B. Sự nhiễu xạ ánh sáng.	C. Sự tán sắc ánh sáng.	D. Sự giao thoa ánh sáng.
Câu 22.	 Trong không khí, khi chiếu ánh sáng có bước sóng 550 nm vào một chất huỳnh quang thì chất này có thể phát ra ánh sáng huỳnh quang có bước sóng là
*A. 650 nm.	B. 450 nm.	C. 480 nm.	D. 540 nm.
Câu 23.	 Trong chân không bức xạ có bước sóng nào sau đây là bức xạ hồng ngoại
*A. 900nm.	B. 250nm.	C. 600nm.	D. 450nm.
Câu 24.	 Trong thí nghiệm Y-âng về giao thoa ánh sáng, khoảng cách giữa hai khe là 1 mm, khoảng cách từ mặt phẳng chứa hai khe đến màn quan sát là 2 m. Nguồn sáng đơn sắc có bước sóng 0,45 μm. Khoảng cách giữa hai vân tối liên tiếp trên màn bằng
*A. 0,9 mm.	B. 1,8 mm.	C. 0,225 mm.	D. 0,45 mm.
Câu 25.	 Trong miền ánh sáng nhìn thấy, chiết suất của nước có giá trị nhỏ nhất đối với ánh sáng đơn sắc nào sau đây?
A. Ánh sáng chàm.	B. Ánh sáng vàng.	*C. Ánh sáng đỏ.	D. Ánh sáng tím.
Câu 26.	 Hiện tượng chùm ánh sáng trắng đi qua lăng kính, bị phân tách thành các chùm sáng đơn sắc là hiện tượng
A. phản xạ ánh sáng.	B. phản xạ toàn phần.	*C. tán sắc ánh sáng.	D. giao thoa ánh sáng.
Câu 27.	 Đại lượng nào sau đây đặc trưng cho mức độ bền vững của hạt nhân?
A. Năng lượng liên kết.		*B. Năng lượng liên kết riêng.
C. Độ hụt khối.		D. Năng lượng nghỉ.
Câu 28.	 Trong thí nghiệm Y-ăng về giao thoa ánh sáng đơn sắc, khoảng cách giữa 7 vân sáng liên tiếp trên màn quan sát là 4,5 mm. Khoảng vân trên màn là
A. 0,64 mm	*B. 0,75 mm	C. 0,9 mm	D. 1,5 mm
Câu 29.	 Một đám nguyên tử hiđrô đang ở trạng thái cơ bản. Khi chiếu bức xạ có tần số f$_1$ vào đám nguyên tử này thì chúng phát ra tối đa 3 bức xạ. Khi chiếu bức xạ có tần số f$_2$ vào đám nguyên tử này thì chúng phát ra tối đa 10 bức xạ. Biết năng lượng ứng với các trạng thái dừng của nguyên tử hiđrô được tính theo biểu thức E$_n$=-$\frac{E_0}{n^2}$  (E$_0$ là hằng số dương, n = 1,2,3,...). Tỉ số $\frac{\text{f}_1}{\text{f}_2}$  là
A. $\frac{10}{3}$	B. $\frac{27}{25}$	C. $\frac{3}{10}$	*D. $\frac{25}{27}$
Câu 30.	Số prôtôn có trong hạt nhân $_{82}^{206}$Pb là
A. 288	*B. 82	C. 206	D. 124
Câu 31.	 Trong thí nghiệp Y-âng về giao thoa với ánh sáng đơn sắc, khoảng cách giữa hai khe là 1mm, khoảng cách từ mặt phẳng chứa hai khe đến màn quan sát là 2m. Tại điểm M trên màn quan sát cách vân sáng trung tâm 3mm có vân sáng bậc 3. Bước sóng của ánh sáng dùng trong thí nghiệm là
A. 0,6 𝜇m.	*B. 0,5 𝜇m.	C. 0,45 𝜇m.	D. 0,75 𝜇m.
Câu 32.	 Hạt nhân có độ hụt khối càng lớn thì có
A. năng lượng liên kết càng nhỏ.	*B. năng lượng liên kết càng lớn.
C. năng lượng liên kết riêng càng lớn.	D. năng lượng liên kết riêng càng nhỏ
Câu 33.	 Trong thí nghiệm Y-âng về giao thoa ánh sáng, hai khe hẹp cách nhau 0,6mm và cách màn quan sát 1,2m. Chiếu sáng các khe bằng ánh sáng đơn sắc có bước sóng λ (380nm <λ< 760nm). Trên màn, điểm M cách vân trung tâm 2,3 mm là vị trí của một vân tối. Giá trị của λ gần nhất với giá trị nào sau đây?
A. 545 nm.	*B. 465nm.	C. 625nm.	D. 385 nm.
Câu 34.	 Cho khối lượng của hạt nhân $_{47}^{107}$Ag là 106,8783u; của nơtron là 1,0087u; của prôtôn là 1,0073u. Độ hụt khối của hạt nhân $_{47}^{107}$Ag là
*A. 0,9868u.	B. 0,6986u.	C. 0,6868u.	D. 0,9686u.
Câu 35.	 Cho h = 6,625.10$^{-34}$ (J.s), c = 3.10$^8$ (m/s), 1 eV = 1,6.10$^{-19}$ (J). Trong chân không, một ánh sáng có bước sóng là 0,60 𝜇m. Năng lượng của phôtôn ánh sáng này bằng
A. 5,14 eV.	B. 4,07 eV.	*C. 2,07 eV.	D. 3,34 eV.
Câu 36.	 Trong thí nghiệm Y-âng về giao thoa ánh sáng, nguồn sáng phát sáng phát ra ánh sáng đơn sắc có bước sóng λ. Trên màn quan sát, vân sáng bậc 2 xuất hiện tại vị trí có hiệu đường đi của ánh sáng từ hai khe đến đó bằng
A. 0,5λ.	B. λ.	*C. 2λ.	D. 1,5λ.
Câu 37.	 Cho phản ứng hạt nhân $_1^3$T + X → $_2^4$He + n, hạt nhân X là hạt nhân nào sau đây?
A.  H	*B.  D	C.  T	D.  He.
Câu 38.	 Theo thuyết lượng tử ánh sáng, phát biểu nào dưới đây là sai?
A. Ánh sáng được tạo thành bởi các hạt gọi là phôtôn.
B. Trong chân không, các phôtôn bay dọc theo tia sáng với tốc độ c = 3.10$^8$ m/s.
C. Phân tử, nguyên tử phát xạ hay hấp thụ ánh sáng, cũng có nghĩa là chúng phát xạ hay hấp thụ phôtôn.
*D. Năng lượng của các phôtôn ánh sáng là như nhau, không phụ thuộc tần số của ánh sáng.
Câu 39.	 Trong thí nghiệm Y-âng về giao thoa với ánh sáng đơn sắc có bước sóng λ, khoảng cách giữa hai khe hẹp là a, khoảng cách từ mặt phẳng chứa hai khe hẹp đến màn quan sát là 2m. Trên màn quan sát, tại điểm M cách vân sáng trung tâm 6 mm, có vân sáng bậc 5. Khi thay đổi khoảng cách giữa hai khe hẹp một đoạn bằng 0,2 mm sao cho vị trí vân sáng trung tâm không thay đổi thì tại M có vân sáng bậc 6. Giá trị của λ bằng
A. 0,55 𝜇m.	B. 0,45 𝜇m.	C. 0,50 𝜇m.	*D. 0,60 𝜇m.
Câu 40.	 Hiện tượng ánh sáng làm bật các electron ra khỏi bề mặt kim loại gọi là
*A. hiện tượng quang điện ngoài.	B. hiện tượng quang phát quang.
C. hiện tượng giao thoa ánh sáng.	D. hiện tượng quang điện trong.
